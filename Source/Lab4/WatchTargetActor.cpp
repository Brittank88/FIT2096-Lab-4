// Fill out your copyright notice in the Description page of Project Settings.


#include "WatchTargetActor.h"

// Sets default values
AWatchTargetActor::AWatchTargetActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> ChairMeshObj(TEXT("/Game/StarterContent/Props/SM_Chair.SM_Chair"));

	if (ChairMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(ChairMeshObj.Object);
	}
}

// Called when the game starts or when spawned
void AWatchTargetActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWatchTargetActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (WatchTarget != nullptr) {
		FVector ToTarget = GetActorLocation() - WatchTarget->GetActorLocation();
		ToTarget.Normalize();

		SetActorRotation(
			FQuat(
				FRotator(
					0,
					FMath::RadiansToDegrees(
						FMath::Atan2(
							FVector::DotProduct(ToTarget, FVector::ForwardVector),
							ToTarget.X * FVector::ForwardVector.Y + ToTarget.Y * FVector::ForwardVector.X
						)
					),
					0
				)
			)
		);
	}
}

