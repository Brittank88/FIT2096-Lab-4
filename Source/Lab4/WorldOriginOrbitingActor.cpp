// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldOriginOrbitingActor.h"

// Sets default values
AWorldOriginOrbitingActor::AWorldOriginOrbitingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshObj(TEXT("/Game/Geometry/Meshes/1M_Cube.1M_Cube"));

	if (CubeMeshObj.Succeeded())
	{
		VisibleComponent->SetStaticMesh(CubeMeshObj.Object);
	}

	RotationSpeed = 20.f;
	RotationDegrees = 0;
	OrbitLocation = FVector::ZeroVector;
}

// Called when the game starts or when spawned
void AWorldOriginOrbitingActor::BeginPlay()
{
	Super::BeginPlay();

	OrbitLocation = FVector(GetWorld()->OriginLocation);
	
	OrbitLocation.SetComponentForAxis(EAxis::Z, GetActorLocation().Z);

	RotationRadius = GetActorLocation() - OrbitLocation;
}

// Called every frame
void AWorldOriginOrbitingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotationDegrees += RotationSpeed * DeltaTime;
	if (RotationDegrees > 360) RotationDegrees -= 360;
	SetActorLocation(OrbitLocation + RotationRadius.RotateAngleAxis(RotationDegrees, FVector(0, 0, 1)));

	FVector ToOrigin = FVector(GetWorld()->OriginLocation) - GetActorLocation();
	ToOrigin.Normalize();
	SetActorRotation(
		FQuat(
			FRotator(
				0,
				FMath::RadiansToDegrees(
					FMath::Atan2(
						ToOrigin.X * FVector::ForwardVector.Y + ToOrigin.Y * FVector::ForwardVector.X,
						FVector::DotProduct(ToOrigin, FVector::ForwardVector)
					)
				),
				0
			)
		)
	);
}

