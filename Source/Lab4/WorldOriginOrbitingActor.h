// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "WorldOriginOrbitingActor.generated.h"

UCLASS()
class LAB4_API AWorldOriginOrbitingActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldOriginOrbitingActor();

	UPROPERTY(EditAnywhere)
		float RotationSpeed;

	UPROPERTY(EditAnywhere)
		FVector OrbitLocation;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent *VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FVector RotationRadius;

	float RotationDegrees;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
